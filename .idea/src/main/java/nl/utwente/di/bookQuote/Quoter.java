package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String isbn){
        HashMap<String, Double> result = new HashMap<>();
        result.put("1", 10.0);
        result.put("2", 45.0);
        result.put("3", 20.0);
        result.put("4", 35.0);
        result.put("5", 50.0);
        if(result.containsKey(isbn)) {
            return result.get(isbn);
        }
        else{
            return 0.0;
        }
    }
}
